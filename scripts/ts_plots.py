import pandas as pd 
import numpy as np

import matplotlib.pyplot as plt
import seaborn as sns

my_cp = plt.rcParams['axes.prop_cycle'].by_key()['color']

def model_performance_lin_plot(y_true, y_pred, title = None):
    ''' model_performance_lin_plot
        plotting routine that shows a linear dependence 
        between true value of y and its prediction
    '''
    # Get min and max values of the predictions and labels
    vals = y_true.append(y_pred).values
    min_val = max(vals)
    max_val = min(vals)
    # Create dataframe with predicitons and labels.
    df = pd.DataFrame({'y_true':y_true,'y_pred':y_pred})

    sns.jointplot(x='y_true',y='y_pred',data=df, kind="reg", height=5)
    plt.plot([min_val, max_val], [min_val, max_val], 'm--')
    plt.title(title)
    plt.show(block=False)

def plot_trips_weather(trips_ext, weather, weather_col, legend_label = None):
    ''' plot_trips_weather
        plotting routine used to compare total number of trips with weather features
    '''

    if legend_label is None:
        legend_label = weather_col

    fig, ax = plt.subplots(5, 1, figsize=(18,25))

    i = 0
    for city in weather.City.unique():
        ax1 = ax[i]
        ax1.set_title(city)
        ax2 = ax[i].twinx()
        i += 1
        
        trips_city_slice = trips_ext.loc[trips_ext['Start City'] == city].groupby('Date_ext')['Trip ID'].count()
        weather_city_slice = weather.loc[weather['City'] == city].set_index('Date',drop=True)[weather_col]
        trips_city_slice = trips_city_slice.resample('1D').asfreq().fillna(0)
        trips_city_slice.plot(color=my_cp[0],ax=ax1)
        weather_city_slice.plot(color=my_cp[1],ax=ax2)
        ax1.legend(['n. trips'],loc=2)
        ax2.legend([legend_label],loc=1)

    plt.show()


def heatMap(df,my_figsize=(6,8),annotate_flag=True):
    '''heatMap
    Returns the correlation matrix of the features of the input dataframe df
    
    Parameters
    ---------- 
    df :  input dataframe
    my_figsize :  tuple, width and height of the figure
    annotate_flag :  bool, annotate switch
    ---------- 
    '''
    corr = df.corr(method='pearson')
    mask = np.array(corr)
    mask[np.tril_indices_from(mask)] = False
    colormap = sns.diverging_palette(220, 10, as_cmap=True)    
    fig, ax = plt.subplots(figsize=my_figsize)
    sns.heatmap(corr, cmap=colormap, annot=annotate_flag,mask = mask,square=True, 
                linewidths=.5, fmt='.2f', annot_kws={'size': 8},
                cbar_kws={"shrink": .6},ax=ax)
    plt.title('High-correlation Features')
    plt.show()


    
def plotCoefficients(model,X_train):
    """plotCoefficients
        Plots sorted coefficient values of the model
    """
    
    coefs = pd.DataFrame(model.coef_, X_train.columns)
    coefs.columns = ["coef"]
    coefs["abs"] = coefs.coef.apply(np.abs)
    coefs = coefs.sort_values(by="abs", ascending=False).drop(["abs"], axis=1)
    
    plt.figure(figsize=(15, 4))
    coefs.coef.plot(kind='bar')
    plt.grid(True, axis='y')
    plt.hlines(y=0, xmin=0, xmax=len(coefs), linestyles='dashed');
