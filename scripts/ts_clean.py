import pandas as pd
import numpy as np

import datetime as dt 

from itertools import product
from pandas.tseries.holiday import USFederalHolidayCalendar

# US Federal holiday calendar
cal = USFederalHolidayCalendar()
holidays = list(cal.holidays(start='2014-09-01', end='2015-09-01').to_pydatetime())

# useful functions for feature extraction
extract_date = lambda x : dt.datetime(x.year,x.month,x.day)
is_weekend = lambda x : int(extract_date(x).weekday() >= 5)
is_holiday = lambda x : int(extract_date(x) in holidays)

# Compute distance in hours from 1Jan1970
since_1Jan1970_hours = lambda x: (x - dt.datetime(1970,1,1,0,0)).seconds // 3600

# Compute distance in days from 1Jan1970
since_1Jan1970_days = lambda x: (x - dt.datetime(1970,1,1)).days

Y = 2000 # dummy leap year to allow input X-02-29 (leap day)
seasons = [('winter', (dt.datetime(Y,  1,  1),  dt.datetime(Y,  3, 20))),
           ('spring', (dt.datetime(Y,  3, 21),  dt.datetime(Y,  6, 20))),
           ('summer', (dt.datetime(Y,  6, 21),  dt.datetime(Y,  9, 22))),
           ('autumn', (dt.datetime(Y,  9, 23),  dt.datetime(Y, 12, 20))),
           ('winter', (dt.datetime(Y, 12, 21),  dt.datetime(Y, 12, 31)))]

def get_season(now):
    ''' get_season
    Parameters
    ----------
    now :  timestamp, datetime.datetime, date
    ----------

    Output
    ----------
    str, season
    ----------
    '''
    now = dt.datetime(Y,now.month,now.day)
    return next(season for season, (start, end) in seasons if start <= now <= end)



def concat_dummies(df,cat_col_name,my_prefix=None,drop_first_flag=False):
    ''' concat_dummies
    Parameters
    ----------
    df :  pd.DataFrame
    cat_col_name :  str, name of the col to one-hot-encode
    my_prefix :  str, neme prefix string
    drop_first_flag :  bool, wether to drop cat_col_name
    ----------

    Output
    ----------
    pd.DataFrame with dummy columns
    ----------
    '''    
    dummies = pd.get_dummies(df[cat_col_name], prefix=my_prefix, drop_first=drop_first_flag)
    return pd.concat([df,dummies],axis=1).drop(cat_col_name,axis=1)

def get_crowfly_distance(x):
    ''' get_crowfly_distance
    Function to calculate distance based on latitude,longitude of 2 places
    using Haversine formula
    '''
    
    lat1 = x[0]*np.pi/180
    lon1 = x[1]*np.pi/180
    lat2 = x[2]*np.pi/180
    lon2 = x[3]*np.pi/180

    dlon = lon2 - lon1 
    dlat = lat2 - lat1 

    R = 6373 # km, 3961 # miles
    a = (np.sin(dlat/2))**2 + np.cos(lat1) * np.cos(lat2) * (np.sin(dlon/2))**2 
    c = 2 * np.arctan2( np.sqrt(a), np.sqrt(1-a) ) 
    d = R * c
    return d

def clean_stations_df(stations):
    ''' clean_stations_df
    Parameters
    ----------
    stations = pd.read_csv("./data/station_data.csv")
    ----------
    '''
    return stations.set_index('Id',drop=True)

def clean_trips_df(trips,stations):
    ''' clean_trips_df
    Parameters
    ----------
    trips = pd.read_csv("./data/trip_data.csv")
    stations :  pd.DataFrame, modified by clean_stations_df
    ----------
    '''
    trips = concat_dummies(trips,'Subscriber Type')

    # Format Start and End Dates
    trips['Start Date'] = pd.to_datetime(trips['Start Date'], format="%d/%m/%Y %H:%M")

    trips['month'] = trips['Start Date'].apply(lambda x:x.month)
    trips['hour'] = trips['Start Date'].apply(lambda x:x.hour)
    trips['weekday'] = trips['Start Date'].dt.dayofweek

    trips['End Date'] = pd.to_datetime(trips['End Date'], format="%d/%m/%Y %H:%M")

    # Duration of the trip in minutes
    trips['duration(m)'] = (trips['End Date'] - trips['Start Date']).apply(lambda x:x.seconds // 60)

    # Retrieve Start and End City given the station ID
    trips['Start Station Name'] = trips['Start Station'].apply(lambda x:stations.loc[x,'Name'])
    trips['End Station Name'] = trips['End Station'].apply(lambda x:stations.loc[x,'Name'])

    # Retrieve Start and End City given the station ID
    trips['Start City'] = trips['Start Station'].apply(lambda x:stations.loc[x,'City'])
    trips['End City'] = trips['End Station'].apply(lambda x:stations.loc[x,'City'])

    # Retrieve Start and End Dock Count
    trips['Start Dock count'] = trips['Start Station'].apply(lambda x:stations.loc[x,'Dock Count'])
    trips['End Dock count'] = trips['End Station'].apply(lambda x:stations.loc[x,'Dock Count'])

    # Get the crowfly distance between Start and End Station GPS coordinates
    st_cols = ['Start Station','End Station']

    trip_geo = pd.DataFrame()
    trip_geo['Start Lat'] = trips['Start Station'].apply(lambda x:stations.loc[x,'Lat'])
    trip_geo['Start Long'] = trips['Start Station'].apply(lambda x:stations.loc[x,'Long'])
    trip_geo['End Lat'] = trips['End Station'].apply(lambda x:stations.loc[x,'Lat'])
    trip_geo['End Long'] = trips['End Station'].apply(lambda x:stations.loc[x,'Long'])

    get_distance = lambda x:get_crowfly_distance(stations.loc[x[0],'Lat'],
                                                stations.loc[x[0],'Long'],
                                                stations.loc[x[1],'Lat'],
                                                stations.loc[x[1],'Long'])

    trips['distance(km)'] = trip_geo.apply(lambda x:get_crowfly_distance(x),axis = 1)
    return trips.sort_values('Start Date', ascending=True)

def construct_signed_df(trips):
    ''' construct_signed_df
    Parameters
    ----------
    trips :  pd.DataFrame, modified by clean_trips_df
    ----------

    Output
    ----------
    pd.DataFrame, with columns 
                -   'date' : dt.datetime, date of the event
                -   'st_id' : int, station ID
                -   net rate : int, n. of arrival/departures at every minute of the trip dataframe 
                                signed with - for departures and + for arrivals
    ----------
    '''
    starts = trips.groupby(['Start Station','Start Date'])['Trip ID'].count().reset_index()
    ends = trips.groupby(['End Station','End Date'])['Trip ID'].count().reset_index()

    dict_col_names_st = {'Start Station': 'st_id', 'Start Date': 'date', 'Trip ID': 'count'}
    dict_col_names_en = {'End Station': 'st_id', 'End Date': 'date', 'Trip ID': 'count'}

    # bikes that leave give negative contribution, bikes that arrive a positive one
    signed_starts = pd.concat([starts[['Start Station','Start Date']],-ends['Trip ID']],axis=1).rename(columns=dict_col_names_st)
    signed_ends = ends.rename(columns=dict_col_names_en)

    # Merge signed_starts, signed_ends
    signed_df = signed_starts.append(signed_ends).sort_values(by=['st_id', 'date']).reset_index().drop('index',axis=1)
    return signed_df

# Remember the following station change:
# 23 -> 85
# 25 -> 86
# 49 -> 87
# 69 -> 88
# 72 -> 89 ->  90

def my_new_index(st_id,agg_df,my_freq='60Min'):
    ''' my_new_index
    Upon resampling the net_rate time series, fixes indices 
    for stations that change name/location
    '''
    if st_id in [23, 25, 49, 69, 72]:
        new_index = pd.date_range('2014-09-01 00:00:00', max(agg_df.index), freq=my_freq)
    elif st_id in [85, 86, 87, 88, 90]:
        new_index = pd.date_range(min(agg_df.index), '2015-08-31 23:45:00', freq=my_freq)
    elif st_id in [89]:
        new_index = pd.date_range(min(agg_df.index), max(agg_df.index), freq=my_freq)
    else:
        new_index = pd.date_range('2014-09-01 00:00:00', '2015-08-31 23:50:00', freq=my_freq)
    return new_index

def construct_lag_df(starts_ends_df,lag='60Min'):
    ''' construct_lag_df
    Parameters
    ----------
    starts_ends_df :  pd.DataFrame, as in either construct_signed_df or construct_starts_ends_df
    lag :  str, one among '10Min','15Min','20Min','30Min','60Min'
            lag of the time resampling
    ----------

    Output
    ----------
    pd.DataFrame, resamples starts_ends_df by the assigned lag and sums over it 
        in order to obtain the function we would like to estimate, 
        the net rate of arrivals/departures at every station. 
        Returns a dataframe with station ID's indexing their net rate on the columns, 
        rows are indexed by time from 2014-09-01 00:00 to 2015-09-01 00:00 with the assigned lag
    ----------
    '''
    if not lag in ['10Min','15Min','20Min','30Min','60Min']:
        raise ValueError('lag out of bounds. Insert a value among 10Min,15Min,20Min,30Min,60Min')

    df = pd.DataFrame()
    for st_id in starts_ends_df['st_id'].unique():
        agg_df = starts_ends_df.loc[starts_ends_df['st_id'] == st_id].groupby('date').agg({'st_id':'first','count':'sum'})
        agg_df = agg_df.resample(rule=lag).sum()
        agg_df = agg_df.reindex(index=my_new_index(st_id,agg_df,lag))
        agg_df['count'].fillna(value=0,inplace=True)
        df[st_id] = agg_df['count']

    # fix station number
    df[23] = df[[23,85]].sum(axis=1)
    df[25] = df[[25,86]].sum(axis=1)
    df[49] = df[[49,87]].sum(axis=1) 
    df[69] = df[[69,88]].sum(axis=1) 
    df[72] = df[[72,89,90]].sum(axis=1)
    df.drop([85,86,87,88,89,90],axis=1,inplace=True)
    return df


def get_cluster_df(trips):
    ''' get_cluster_df
    Parameters
    ----------
    trips :  pd.DataFrame, modified by clean_trips_df
    ----------

    Output
    ----------
    pd.DataFrame, uses resampled signed df by hour and grouped by hour of the day, 
        in order to cluster stations
    ----------
    '''
    signed_df = construct_signed_df(trips)
    df = construct_lag_df(signed_df,'60Min')
    df['hour'] = df.index.hour
    return df.groupby('hour').mean()

def get_cluster_label_df(labels, cluster_df):
    ''' get_cluster_label_df
    Parameters
    ----------
    labels :  KMeans(...).labels_
    station_indices :  stations.index 
    ----------

    Output
    ----------
    pd.DataFrame, with station ID as row indices and cluster column as single column 
    ----------
    '''
    dflabel = pd.DataFrame({'label': labels}, index=cluster_df.columns)
    dflabel['label'] = dflabel.label.astype(int)
    dflabel.loc[85,'label'] = dflabel.loc[23,'label']
    dflabel.loc[86,'label'] = dflabel.loc[25,'label']
    dflabel.loc[87,'label'] = dflabel.loc[49,'label']
    dflabel.loc[88,'label'] = dflabel.loc[69,'label']
    dflabel.loc[89,'label'] = dflabel.loc[72,'label']
    dflabel.loc[90,'label'] = dflabel.loc[72,'label']
    return dflabel

def get_station_label_df(stations, trips, labels):
    ''' get_station_label_df
    Parameters
    ----------
    stations :  pd.DataFrame, modified by clean_stations_df
    trips :  pd.DataFrame, modified by clean_trips_df
    labels :  KMeans(...).labels_
    ----------

    Output
    ----------
    pd.DataFrame, merges station and labels, and assigns labels 
        according to [cluster label] x [city]
    ----------
    '''

    cluster_df = get_cluster_df(trips)
    dflabel = get_cluster_label_df(labels, cluster_df)

    station_label_df = stations.merge(dflabel, right_index=True, left_index=True, how='left')
    station_label_df['label'] = station_label_df.label.astype(int)
    station_label_df['City-label'] = station_label_df[['City','label']].apply(lambda x:(x[0],x[1]),axis=1)

    cities = list(station_label_df['City'].unique())
    labels = [0,1]
    unique_new_labels = list(product(cities,labels))
    station_label_df['label'] = station_label_df['City-label'].apply(lambda x:unique_new_labels.index(x))
    station_label_df.drop('City-label',axis=1,inplace=True)
    station_label_df['label'] = station_label_df['label'].astype(int)
    return station_label_df


def clean_weather_df(weather_in,drop_col=True):
    ''' clean_weather_df
    weather_in = pd.read_csv("./data/weather_data.csv")
    drop_col :  bool, whether to drop columns that are too correlated
    '''
    weather = weather_in.copy()

    zip_codes_dict = {
        94107 : 'San Francisco', 
        94063 : 'Redwood City', 
        94301 : 'Palo Alto', 
        94041 : 'Mountain View', 
        95113 : 'San Jose'
    }

    # Find city from zip code of the forecast
    weather['City'] = weather['Zip'].apply(lambda x:zip_codes_dict[x])
    weather.drop(['Zip'],inplace=True,axis=1)

    # Process dates
    weather['Date'] = pd.to_datetime(weather['Date'],format='%d/%m/%Y')
    
    # Fix some column names
    weather.rename(columns={'MeanDew PointF': 'Mean Dew PointF', 'Min DewpointF': 'Min Dew PointF'},inplace=True)

    weather['Max Gust SpeedMPH'].fillna(0, inplace=True)
    weather['Events'].fillna(value='Normal',inplace=True)

    cat_cols = ['Date','CloudCover', 'WindDirDegrees','City']
    float_cols = [col for col in weather.columns if not col in cat_cols]

    # fills null values column by column by linear interpolation (use cubic?)
    for city in weather.City.unique():
        city_slice = weather.loc[weather.City == city]
        weather.loc[weather.City == city,float_cols] = city_slice.interpolate(method='linear',limit_direction='both')

    na_date = weather[weather['CloudCover'].isna()]['Date'].values[0]
    na_city = weather[weather['CloudCover'].isna()]['City'].values[0] # Palo Alto

    # Assume Cloud Cover and Wind direction in Palo Alto is same as in Mountain View (neighbor city)
    my_slice_na = (weather.City == na_city) & (weather.Date == na_date)
    my_slice_copy = (weather.City == 'Mountain View') & (weather.Date == na_date)
    weather.loc[my_slice_na,'CloudCover'] = weather.loc[my_slice_copy,'CloudCover'].values[0]
    weather.loc[my_slice_na,'WindDirDegrees'] = weather.loc[my_slice_copy,'WindDirDegrees'].values[0]

    if drop_col:
        weather['CloudCover'] = weather.CloudCover.astype('int')
        weather = concat_dummies(weather,'CloudCover','cloud')
        weather = concat_dummies(weather,'Events')

        #weather['Rain'] = weather['Rain'] + weather['Fog-Rain'] +  weather['Rain-Thunderstorm']
        weather['Fog'] = weather['Fog'] + weather['Fog-Rain']

        # We drop columns 'Fog-Rain' and 'Rain-Thunderstorm' 
        # because they are respectively 16 and 3 days over 365 
        # (we merge their info in 'Rain' and 'Fog')
        # We also drop 'Rain' because it is highly related to PrecipitationIn and Humidity
        drop_col_list = ['Max TemperatureF','Min TemperatureF',
                        'Max Dew PointF','Mean Dew PointF','Min Dew PointF',
                        'Max Humidity','Min Humidity',
                        'Max Sea Level PressureIn','Min Sea Level PressureIn',
                        'Mean VisibilityMiles', 'Max Wind SpeedMPH',
                        'Max Gust SpeedMPH',#
                        'Normal','Fog-Rain','Rain-Thunderstorm',#]
                        'Rain'
                        ]

        weather.drop(drop_col_list,axis=1,inplace=True)
        weather.drop('WindDirDegrees',axis=1,inplace=True)

        weather_col_name_dict = {
            'Mean TemperatureF':'Temp',
            #'Min Dew PointF':'Dew',
            'Mean Humidity':'Humid',
            'Mean Sea Level PressureIn':'Pressure',
            'Max VisibilityMiles':'MaxVis',
            'Min VisibilityMiles':'MinVis',
            'Mean Wind SpeedMPH':'Wind',
            #'Max Gust SpeedMPH':'Gust',
            'PrecipitationIn':'Prec',
            #'WindDirDegrees':'WindDir',
        }

        return weather.rename(columns = weather_col_name_dict)
    else:
        return weather


def construct_starts_ends_df(trips):
    ''' construct_starts_ends_df
    Parameters
    ----------
    trips :  pd.DataFrame, obtained from clean_trips_df
    ----------

    Outputs
    ----------
    tuple of two pd.DataFrame, datasets of arrivals/departures
    ----------
    '''
    starts = trips.groupby(['Start Station','Start Date'])['Trip ID'].count().reset_index()
    ends = trips.groupby(['End Station','End Date'])['Trip ID'].count().reset_index()

    dict_col_names_starts = {'Start Station': 'st_id', 'Start Date': 'date', 'Trip ID': 'count'}
    starts = starts.rename(columns=dict_col_names_starts)
    starts.name = 'bikes_leaving'

    dict_col_names_end = {'End Station': 'st_id', 'End Date': 'date', 'Trip ID': 'count'}
    ends = ends.rename(columns=dict_col_names_end)
    starts.name = 'bikes_arriving'
    return starts, ends

def merge_dataset(trips,weather,merge_stations_labels,lag='60Min'):
    ''' merge_dataset
    Parameters
    ----------
    trips :  pd.DataFrame, obtained from clean_trips_df
    weather :  pd.DataFrame, obtained from clean_weather_df
    merge_stations_labels :  pd.DataFrame, obtained from get_station_label_df
                                tw :  int, time window (in days) for feature extraction
    lag :  str, one among '10Min','15Min','20Min','30Min','60Min', 
        to be used in construct_lag_df for time resampling
    ----------

    Output
    ----------
    list of pd.DataFrame, list of merged datasets, with number of arrivals/departures averaged per cluster, 
        with weather information
    ----------
    '''

    starts, ends = construct_starts_ends_df(trips)

    df_out = construct_lag_df(starts,lag)
    df_in = construct_lag_df(ends,lag)
    label_series = merge_stations_labels.reset_index().groupby('label')['Id'].apply(list)
    city_label = merge_stations_labels.reset_index().groupby('label')['City'].first()

    avg_df = []
    for df in [df_out,df_in]:
        avg_df_temp = []
        for label in label_series.index:
            indices = [i for i in label_series[label] if i not in [85,86,87,88,89,90]]
            cluster_nr = df[indices].mean(axis=1)
            cluster_nr.name = 'avg_y'
            
            cl_df = pd.DataFrame(cluster_nr)
            scale = 60 // int(lag[:2])

            cl_df['avg_y_7'] = cl_df['avg_y'].shift(24*7*scale)

            cl_df['city'] = city_label[label]
            cl_df['date_ext'] = cl_df.index.map(extract_date)

            cl_df['weekend'] = cl_df.index.map(is_weekend).astype('bool')
            cl_df['holiday'] = cl_df.index.map(is_holiday).astype('bool')
            cl_df['season'] = cl_df.index.map(get_season)

            l_indices = ['date_ext','city']
            r_indices = ['Date','City']
            my_merge = cl_df.merge(weather, left_on=l_indices, right_on=r_indices, how='left')
            df_weather = my_merge.drop(l_indices+r_indices, axis=1)
            df_weather.index = cl_df.index

            h = cl_df.index.map(since_1Jan1970_hours)
            for i in range(1,4):
                df_weather['sinH_{}'.format(i)] = np.sin(h*(i*2.*np.pi/24))
                df_weather['cosH_{}'.format(i)] = np.cos(h*(i*2.*np.pi/24))

            d = cl_df.index.map(since_1Jan1970_days)
            for i in range(1,4):
                df_weather['sinD_{}'.format(i)] = np.sin(d*(i*2.*np.pi/7))
                df_weather['cosD_{}'.format(i)] = np.cos(d*(i*2.*np.pi/7))

            df_weather = concat_dummies(df_weather,'season')

            avg_df_temp.append(df_weather.dropna())
        avg_df.append(avg_df_temp)
    return avg_df[0], avg_df[1]


def my_train_test_split(periods,y,X = None,subset_months=None):
    """my_train_test_split, file ts_utils.py
    Split train and test sets, given number of months in test set
    """
    if not subset_months is None:
        X = X.loc[subset_months]
        y = y.loc[subset_months]
    split_train = y.index < y.index[-periods]
    split_test = y.index >= y.index[-periods]
    y_train = y.loc[split_train]
    y_test = y.loc[split_test]
    y_train.name = 'y_train'
    y_test.name = 'y_test'
    if not X is None:
        X_train = X.loc[split_train]
        X_test = X.loc[split_test]
        X_train.name = 'X_train'
        X_test.name = 'X_test'
        return X_train, X_test, y_train, y_test
    return y_train, y_test