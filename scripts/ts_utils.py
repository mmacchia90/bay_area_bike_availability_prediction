import pandas as pd 
import numpy as np

from sklearn.model_selection import RandomizedSearchCV, GridSearchCV
from sklearn.pipeline import Pipeline

from sklearn.decomposition import PCA

from sklearn.metrics import make_scorer
from sklearn.externals import joblib

from sklearn.model_selection import TimeSeriesSplit 

from sklearn.metrics import mean_squared_error

from ts_clean import (my_train_test_split,concat_dummies)
from ts_plots import model_performance_lin_plot

def mean_absolute_percentage_error(y_true, y_pred):
    """mean_absolute_percentage_error
        Calculates MAPE given y_true and y_pred
    """
    mask = y_true != 0
    return (100*np.fabs(y_true - y_pred)/y_true)[mask].mean() 

def root_mean_squared_error(y_true, y_pred):
    """root_mean_squared_error
        Calculates RMSE given y_true and y_pred
    """
    return np.sqrt(mean_squared_error(y_true, y_pred))

def print_scores(actual_vals, predicted_vals):
    """Print scores, file ts_stats.py"""
    rmse = ts_utils.root_mean_squared_error(actual_vals, predicted_vals)
    print("RMSE: {:,.4f}".format(rmse))
    #mape = ts_utils.mean_absolute_percentage_error(actual_vals, predicted_vals)
    #print("MAPE: {:,.4f}".format(mape))

def myTimeSeriesSplit(n_rows_y,test_size,train_size=0):
    """myTimeSeriesSplit
    Time Series cross-validator: Provides train/test indices to split time series data samples
    that are observed at fixed time intervals, in train/test sets.
    In each split, test indices are shifted by one to the right w.r.t. before.
    
    Parameters
    ----------
    n_rows_y :  int, number of samples in the 
    test_size :  int, size of the validation set at every step
    train_size :  int, size of the training set at every step
    ----------
    
    Example
    ----------
    test_size = 3
    tscv = myTimeSeriesSplit(y_train.shape[0],test_size)
    for train_index, test_index in tscv:
        print("TRAIN:", train_index, "TEST:", test_index)
    ----------
    """
    for i in range(test_size,n_rows_y-test_size+1):
        if train_size != 0:
            train_index = np.arange(i-train_size,i)
        else:
            train_index = np.arange(0,i)
        test_index = np.arange(i,i+test_size)
        yield (train_index,test_index)


def cross_validate_ts(model,xtrain_full,ytrain_full,test_size,verbose=True,train_days = 7):
    """cross_validate_ts
    Creates a train/test indices to split time series data samples
    that are observed at fixed time intervals, in train/test sets.

    Parameters
    ----------
    model :  optimization model
    xtrain_full :  pd.DataFrame, training set
    ytrain_full :  pd.Series, training set
    test_size :  int, size of the validation set at every step
    verbose :  bool, optional, verbosity flag
    train_years :  int, optional, minimum size of the training set
    ----------
    
    Output
    ----------
    mean_rmses : list, mean validation root_mean_squared_error
    mean_mapes : list, mean validation mean_absolute_percentage_error
    ----------

    Example
    ----------
    model = RandomForestRegressor()
    mean_rmse, mean_mape = cross_validate_ts(model,X_train,y_train,time_window)
    ----------
    """
    xtrain = xtrain_full.iloc[train_days:]
    ytrain = ytrain_full.iloc[train_days:]
    rmses = []
    mapes = []

    tscv = myTimeSeriesSplit(ytrain.shape[0],test_size)
        
    for train_index,test_index in tscv:
        
        X_train_cv = np.array(xtrain_full.iloc[list(range(train_days+train_index[-1]))])
        y_train_cv = ytrain_full.iloc[list(range(train_days+train_index[-1]))]
        X_test_cv = xtrain.iloc[test_index]
        y_test_cv = ytrain.iloc[test_index]
        
        model.fit(X_train_cv, y_train_cv)
        
        y_pred_test_cv = pd.Series(model.predict(X_test_cv),index=y_test_cv.index,name='y_pred_test')
        
        rmses.append(root_mean_squared_error(y_test_cv, y_pred_test_cv))
        mapes.append(mean_absolute_percentage_error(y_test_cv, y_pred_test_cv))
    
    mean_rmses = np.mean(rmses)
    mean_mapes = np.mean(mapes)
    if verbose:
        print('Mean rmse score = {:,.4f}'.format(mean_rmses))
        print('Mean mape score = {:.4f}'.format(mean_mapes))
    return mean_rmses, mean_mapes

def myRandomizedSearchCV(model,xtrain,ytrain,test_size,parameter_dict,verbose=True):
    """myRandomizedSearchCV
    Creates a train/test indices to split time series data samples
    that are observed at fixed time intervals, in train/test sets.

    Parameters
    ----------
    model :  optimization model
    xtrain :  pd.DataFrame, training set
    ytrain :  pd.Series, training set
    test_size :  int, size of the validation set at every step
    
    parameter_dict :  dict, dictionary of parameters for model to gridsearch 
    scaler :  scaler for train data, tested MinMaxScaler(), RobustScaler()
              see https://scikit-learn.org/stable/modules/classes.html#module-sklearn.preprocessing
    ----------
    
    Output
    ----------
    rand_search.best_score_
    rand_search.best_params_ 
    rand_search.best_estimator_ 
    see https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.RandomizedSearchCV.html
    ----------
    
    Example
    ----------
    time_window = 3
    X_train, X_test, y_train, y_test, dfn, X_test_future = tsc.create_split_dataset_wrapper(time_window)
    parameters={        
        'alpha' : np.logspace(1e-6, 2, 400),
        #'fit_intercept' : [True, False],
        #'solver' : ['svd', 'cholesky', 'sparse_cg', 'sag']
    }
    rrg = Ridge()
    N = y_test.shape[0]
    _,_,opt_rrg = tsu.myGridSearchCV(rrg,X_train,y_train,N,parameters)
    ----------
    """

    scorer_rmse = make_scorer(root_mean_squared_error, greater_is_better=False)

    tscv = myTimeSeriesSplit(ytrain.shape[0],test_size)
    rand_search = RandomizedSearchCV(estimator=model, param_distributions=parameter_dict,
                                scoring=scorer_rmse, cv=tscv,n_jobs=-1)
    rand_search.fit(xtrain,ytrain)
    
    if verbose:
        print("Best mean test rmse: {:,.4f}".format(-rand_search.best_score_))
        print("Best parameters for model:", rand_search.best_params_)
    return (-rand_search.best_score_), rand_search.best_params_, rand_search.best_estimator_

def myGridSearchCV(model,xtrain,ytrain,test_size,parameter_dict,scaler=None,use_pca=False,verbose=True):
    """myGridSearchCV
    Creates a train/test indices to split time series data samples
    that are observed at fixed time intervals, in train/test sets.

    Parameters
    ----------
    model :  optimization model
    xtrain :  pd.DataFrame, training set
    ytrain :  pd.Series, training set
    test_size :  int, size of the validation set at every step
    
    parameter_dict :  dict, dictionary of parameters for model to gridsearch 
    scaler :  scaler for train data, tested MinMaxScaler(), RobustScaler()
              see https://scikit-learn.org/stable/modules/classes.html#module-sklearn.preprocessing
    use_pca:  bool, use PCA flag
    
    verbose :  bool, optional, verbosity flag
    ----------
    
    Output
    ----------
    grid_search.best_score_
    grid_search.best_params_ 
    grid_search.best_estimator_ 
    see https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.GridSearchCV.html
    ----------
    
    Example
    ----------
    time_window = 3
    X_train, X_test, y_train, y_test, dfn, X_test_future = tsc.create_split_dataset_wrapper(time_window)
    parameters={        
        'alpha' : np.logspace(1e-6, 2, 400),
        #'fit_intercept' : [True, False],
        #'solver' : ['svd', 'cholesky', 'sparse_cg', 'sag']
    }
    rrg = Ridge()
    N = y_test.shape[0]
    _,_,opt_rrg = tsu.myGridSearchCV(rrg,X_train,y_train,N,parameters)
    ----------
    """
    pipe_list = []
    if not scaler is None:
        pipe_list += [('scl', scaler)]
    if use_pca:
        pipe_list += [('pca', PCA(n_components = 5))]
    pipe_model = Pipeline(pipe_list + [('reg', model)])

    pipe_param_dict = [{'reg__'+key:value for key, value in parameter_dict.items()}]

    scorer_rmse = make_scorer(root_mean_squared_error, greater_is_better=False)

    #tscv = myTimeSeriesSplit(ytrain.shape[0],test_size)
    #tscv = TimeSeriesSplit(n_splits = ytrain.shape[0] // test_size + 1).split(xtrain)
    tscv = TimeSeriesSplit(n_splits = 8).split(xtrain)

    grid_search = GridSearchCV(estimator=pipe_model, param_grid=pipe_param_dict,
                               scoring=scorer_rmse, cv=tscv,n_jobs=-1)
    grid_search.fit(xtrain,ytrain)
    
    if verbose:
        print("Best mean validation rmse: {:,.4f}".format(-grid_search.best_score_))
        print("Best parameters for model:", grid_search.best_params_)
    return (-grid_search.best_score_), grid_search.best_params_, grid_search.best_estimator_

def stacked_predictions(df_list,param_dict_array,model_array,model_dict,
                        tw_hours_val,tw_hours_test,scaler=None,use_pca=False,
                        drop_more_cols = [],plot_flag=True):
    ''' stacked_predictions
    Parameters
    ----------
    df_list :  list of pd.DataFrames, as in merge_dataset (in ts_clean.py)
    param_dict_array :  dict, dictionary of parameters for models to pass to GridSearchCV
    model_array :  dict, dictionary of sklearn models
    model_dict :  dict, dictionary of sklearn model names
    tw_hours_val :  int, number or hours in the validation set
    tw_hours_test :  int, number or hours in the test set
    scaler :  scaler for train data, tested MinMaxScaler(), RobustScaler()
              see https://scikit-learn.org/stable/modules/classes.html#module-sklearn.preprocessing
    use_pca:  bool, use PCA flag
    drop_more_cols :  list, list of features in df_list[i] to further drop (optional)
    plot_flag :  bool, flag to plot linear correlation of validation prediction and true value
    ----------

    Output
    ----------
    X_train_stack,X_test_stack, :  pd.DataFrame, training and test features for the meta-model (stacking)
    y_train_stack,y_test_stack  :  pd.Series, training and test function to predict for the meta-model (stacking)
    ----------
    '''
    X_train_stack = pd.DataFrame()
    y_train_stack = pd.Series()

    X_test_stack = pd.DataFrame()
    y_test_stack = pd.Series()

    for idx in range(len(df_list)):
        X = df_list[idx].drop(drop_more_cols+['avg_y'],axis=1)
        y = df_list[idx]['avg_y']

        X_train, X_val, y_train, y_val = my_train_test_split(tw_hours_val,y[:-tw_hours_test],X[:-tw_hours_test])

        X_test, y_test = X[-tw_hours_test:],y[-tw_hours_test:]

        print('Fold {} - Estimator: {}'.format(idx, model_dict[idx]))

        model = model_array[idx]
        parameter_dict = param_dict_array[idx]

        best_rmse_train,best_param,current_model = myGridSearchCV(model,X_train,y_train,
                                                                  y_test.shape[0],parameter_dict,
                                                                  scaler,use_pca,verbose=False)

        y_pred_train = pd.Series(current_model.predict(X_train),index=y_train.index,name='y_pred_train')
        y_pred_val = pd.Series(current_model.predict(X_val),index=y_val.index,name='y_pred')

        y_pred_test = pd.Series(current_model.predict(X_test),index=y_test.index,name='y_pred')

        print("Best parameters for model:", best_param)
        print('Mean cv score for best params: {:,.4f} '.format(best_rmse_train))

        current_rmse = root_mean_squared_error(y_val, y_pred_val)
        print('Validation rmse: {:,.4f} '.format(current_rmse))

        if plot_flag:
            model_performance_lin_plot(y_val, y_pred_val)

        if idx != len(df_list) - 1:
            print('-----------------------------------------------------------')

        X_val['cl_id'] = idx
        X_test['cl_id'] = idx
        X_train_stack = X_train_stack.append(pd.concat([X_val['cl_id'],y_pred_val],axis=1))
        y_train_stack = y_train_stack.append(y_val)
        X_test_stack = X_test_stack.append(pd.concat([X_test['cl_id'],y_pred_test],axis=1))
        y_test_stack = y_test_stack.append(y_test)

    X_train_stack = concat_dummies(X_train_stack,'cl_id','cl')
    X_test_stack = concat_dummies(X_test_stack,'cl_id','cl')
    X_train_stack.sort_index(inplace=True)
    y_train_stack.sort_index(inplace=True)
    X_test_stack.sort_index(inplace=True)
    y_test_stack.sort_index(inplace=True)

    return X_train_stack,X_test_stack,y_train_stack,y_test_stack


def myGridSearchCV_multi(model_array,xtrain,ytrain,xtest,ytest,test_size,
                            model_dict,parameter_dict_array,scaler=None,use_pca=False,savefile=True,savedir='./'):
    """myGridSearchCV
    Creates a train/test indices to split time series data samples
    that are observed at fixed time intervals, in train/test sets.

    Parameters
    ----------
    model_array :  dict, dictionary of optimization model
    xtrain :  pd.DataFrame, training set
    ytrain :  pd.Series, training set
    xtest :  pd.DataFrame, test set
    ytest :  pd.Series, test set
    test_size :  int, size of the validation set at every step
    model_dict :  dict, dictionary of names of models
    parameter_dict_array : dict, dictionary of dictionaries of parameters for model to gridsearch 
    scaler :  scaler for train data, tested MinMaxScaler(), RobustScaler()
              see https://scikit-learn.org/stable/modules/classes.html#module-sklearn.preprocessing
    use_pca:  bool, PCA
    savefile :  bool, if True, the best model is saved as pkl filein the savedir folder, 
                the name of the model is taken from model_dict
    savedir : str, directory where to save the best model pkl
    ----------
    
    Output
    ----------
    best_y_pred :  Prediction of test values for best params
    ----------
    """
    print('Performing model optimizations...')
    best_rmse_test =  float("inf")
    best_model = ''
    best_idx = 0
    best_y_pred = []
    for idx in range(len(model_array)):
        print('\nEstimator: %s' % model_dict[idx])
        model = model_array[idx]
        parameter_dict = parameter_dict_array[idx]
        
        best_rmse_train,best_param,current_model = myGridSearchCV(model,xtrain,ytrain,test_size,
                                                    parameter_dict,scaler,use_pca,verbose=False)
        
        y_pred = pd.Series(current_model.predict(xtest),index=ytest.index,name='y_pred')
        
        # Test data rmse of model with best params
        current_rmse_test = root_mean_squared_error(ytest, y_pred)
        print("Best parameters for model:", best_param)
        print('Mean test score for best params: {:,.4f} '.format(best_rmse_train))
        print('Validation score for best params: {:,.4f} '.format(current_rmse_test))

        # Track best (highest test accuracy) model
        if current_rmse_test < best_rmse_test:
            best_rmse_test = current_rmse_test
            best_model = current_model
            best_idx = idx
            best_y_pred = y_pred
    print('\nRegressor with best validation score: %s' % model_dict[best_idx])
    print('Best validation score for best model: {:,.4f}'.format(best_rmse_test))

    if savefile:
        if savedir[-1] != '/':
            savedir += '/'
        dump_file = savedir + model_dict[best_idx] + '.pkl'
        joblib.dump(best_model, dump_file, compress=1)
        print('\nSaved %s grid search pipeline to file: %s' % (model_dict[best_idx], dump_file))
    return best_y_pred


